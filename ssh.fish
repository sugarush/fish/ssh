if ! test -f $sugar_install_directory/config/ssh.fish
  cp $argv[1]/config/ssh.fish $sugar_install_directory/config/ssh.fish
end

source $sugar_install_directory/config/ssh.fish

@sugar-load 'ssh-keys-load'

function ssh-key-load -d 'Load a SSH key by name.'
  ssh-add -q $HOME/.ssh/$argv[1]
end

function ssh-key-unload -d 'Unload a SSH key by name.'
  ssh-add -d -q $HOME/.ssh/$argv[1]
end

function ssh-keys-load -d 'Load SSH keys from the ssh_keys array.'
  for key in $ssh_keys
    ssh-key-load $key
  end
end

function ssh-keys-unload -d 'Unload all SSH keys.'
  ssh-add -D -q
end
